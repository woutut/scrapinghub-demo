#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re


# Process all_artists, get only specific artist type if needed
def get_artists(artist_data, artist_type=''):
    if artist_data:
        return [
            y[0]
            for y in [
                re.findall('.*[a-zA-Z]: (.*)', x)
                for x in artist_data.extract_first().split(';')
                if artist_type in x
            ]
            if y
        ]
    else:
        return []


# Process sizes
def get_sizes(dimensions):

    # Get first two dimensions of any dimensions list
    # If less than 2 dimensions - ignore
    found_dimensions = re.findall(
        '\((?: |)([0-9.]*)(?:| )x(?: |)([0-9.]*)[x.0-9 ]*cm\)', dimensions
    )

    # Return width/height data
    if found_dimensions:
        # Get data from first dimensions mention (image size)
        return float(found_dimensions[0][0]), float(found_dimensions[0][1])
    else:
        return None, None


# Replace unnecessary text in category header
def get_header(header):
    return header.extract_first().replace('Browse - ', '')
