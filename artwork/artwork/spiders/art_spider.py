#!/usr/bin/env python
# -*- coding: utf-8 -*-
import scrapy
from artwork.utilities import get_artists, get_sizes, get_header


class ArtSpider(scrapy.Spider):

    name = "art"
    start_urls = [
        'http://pstrial-2018-05-21.toscrape.com/browse/insunsh',
        'http://pstrial-2018-05-21.toscrape.com/browse/summertime',
    ]

    def parse(self, response):

        # Process breadcrumbs
        # Don't update breadcrumbs if same category, but new page
        if not response.meta.get('new_page'):
            response.meta['path'] = response.meta.get('path', []) + [
                get_header(response.css('div#body h1::text'))
            ]

        # Get art (skip nav links)
        art_list = [
            x
            for x in response.css('div#body div:last-child a')
            if 'browse' not in x.css('::attr(href)').extract_first()
        ]

        # Process art pages
        for art in art_list:
            request = response.follow(art, self.parse_art)
            # Send category name
            request.meta['path'] = response.meta['path']
            yield request

        # Get next page link
        next_page = response.css(
            'div#body div:last-child a:last-child::attr(href)'
        ).extract_first()

        # If art detected - next page
        if art_list:
            request = response.follow(next_page, callback=self.parse)
            request.meta['path'] = response.meta['path']
            request.meta['new_page'] = True
            yield request

        # Process inner categories
        inner_categories = response.css('div#subcats > div > a')
        for category in inner_categories:
            request = response.follow(category, callback=self.parse)
            request.meta['path'] = response.meta['path']
            request.meta['new_page'] = False
            yield request

    # Parse art pages
    def parse_art(self, response):

        # Process sizes
        height, width = get_sizes(
            response.xpath(
                '//dt[contains(text(),"Dimensions")]/following-sibling::dd/text()'
            ).extract_first()
        )

        # Prepare data
        art_data = {
            'url': response.request.url,
            'artists': get_artists(response.css('h2[itemprop="artist"]::text')),
            'title': response.css('h1::text').extract_first(),
            'image': response.urljoin(
                response.css('#body img:first-child::attr(src)').extract_first()
            ),
            'height': height,
            'width': width,
            'description': response.css(
                'div[itemprop="description"] p::text'
            ).extract_first(),
            'path': response.meta['path'],
        }

        # Ommit empty fields and yield
        yield {k: v for k, v in art_data.items() if v}
